import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  DynamoDB,
  PutItemInput
} from '@aws-sdk/client-dynamodb'
import {
  failure,
  success
} from '../common/util'
import {toDynamoItemInput} from '../common/conversion'

const tableName: string = process.env.SPONSORS_TABLE

export const putSponsor = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const input: PutItemInput = toDynamoItemInput(event.body, tableName)
    await dynamoDB.putItem(input)
    return success()
  } catch (e) {
    return e.type === 'ValidationError'
           ? failure(400, 'The input arguments were invalid.')
           : failure(e.message)
  }
}