process.env.SPONSORS_TABLE = 'sponsors'
import {APIGatewayProxyResult} from 'aws-lambda'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {putSponsor} from './putSponsor'
import {
  DynamoSponsor,
  FakeDynamoDB
} from '../mock/dynamoDB'

const SPONSOR = {
  name: 'Some Co.',
  contactName: 'somebody',
  contactEmail: 'some@body.com',
  contactPhone: '0123456789',
  logo: 'http://google.de'
}

describe('PUT /sponsors', () => {
  let result: APIGatewayProxyResult
  const mock: FakeDynamoDB<DynamoSponsor> = new FakeDynamoDB()

  beforeAll(async () => {
    const event: any = {body: JSON.stringify(SPONSOR)}
    result = await putSponsor(mock as unknown as DynamoDB)(event)
  })

  it('should return status 200', async () => {
    expect(result.statusCode).toEqual(200)
  })

  it('should store a valid sponsor', async () => {
    expect(mock.table).toEqual('sponsors')
    expect((await mock.scan({TableName: process.env.SPONSORS_TABLE})).Items[0].name).toEqual({'S': 'Some Co.'})
  })
})