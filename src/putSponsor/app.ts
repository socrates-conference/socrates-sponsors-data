import {putSponsor} from './putSponsor'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

export const handler = putSponsor(new DynamoDB({region: 'eu-central-1'}))