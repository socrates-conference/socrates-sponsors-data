import {
  DynamoDB,
  GetItemCommandOutput,
  ScanCommandOutput
} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {Sponsor} from '../common/types'
import {
  failure,
  success,
  tableName
} from '../common/util'

const fromItem = (item): Sponsor => {
  const sponsor = {}
  Object.entries(item).forEach(([key, value]) => sponsor[key] = value[Object.keys(value)[0]])
  return sponsor as Sponsor
}

export const getSponsors = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const id: string | undefined = event?.pathParameters?.id
  if (id !== undefined) {
    const result: GetItemCommandOutput = await dynamoDB.getItem({TableName: tableName, Key: {'id': {'S': id}}})
    if (result.Item !== undefined) {
      return success(fromItem(result.Item))
    } else {
      return failure(404, `A sponsor with id:${id} does not exist`)
    }
  } else {
    const result: ScanCommandOutput = await dynamoDB.scan({TableName: tableName})
    return success(result.Items.map(fromItem))
  }
}