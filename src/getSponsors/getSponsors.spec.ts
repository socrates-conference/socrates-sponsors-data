process.env.SPONSORS_TABLE = 'sponsors'
import {APIGatewayProxyEvent} from 'aws-lambda'
import {getSponsors} from './getSponsors'
import {
  DynamoSponsor,
  FakeDynamoDB
} from '../mock/dynamoDB'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

describe('GET /sponsors', () => {
  let fakeDynamoDB: FakeDynamoDB<DynamoSponsor>
  beforeEach(() => {
    fakeDynamoDB = new FakeDynamoDB()
  })

  describe('without path parameters', () => {
    it('should return empty list', async () => {
      const result = await getSponsors(fakeDynamoDB as unknown as DynamoDB)({pathParameters: undefined} as APIGatewayProxyEvent)
      expect(result.statusCode).toEqual(200)
      expect(fakeDynamoDB.table).toEqual('sponsors')
      expect(JSON.parse(result.body)).toEqual([])
    })
    describe('when a sponsor exists', () => {
      const SPONSOR: DynamoSponsor = {
        id: {S: '1'},
        name: {S: 'Some Co.'},
        contactName: {S: 'somebody'},
        contactEmail: {S: 'some@body.com'},
        contactPhone: {S: '0123456789'},
        logo: {S: 'http://google.de'}
      }
      beforeEach(() => {
        fakeDynamoDB._items.set(JSON.stringify({id: {S: '1'}}), SPONSOR)
      })

      it('should return list of one', async () => {
        const result = await getSponsors(fakeDynamoDB as unknown as DynamoDB)({} as APIGatewayProxyEvent)
        expect(result.statusCode).toEqual(200)
        expect(fakeDynamoDB.table).toEqual('sponsors')
        expect(JSON.parse(result.body)).toEqual([{
          id: '1',
          name: 'Some Co.',
          contactName: 'somebody',
          contactEmail: 'some@body.com',
          contactPhone: '0123456789',
          logo: 'http://google.de'}])
      })
    })
  })
})