import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {getSponsors} from './getSponsors'

export const handler = getSponsors(new DynamoDB({region: 'eu-central-1'}))