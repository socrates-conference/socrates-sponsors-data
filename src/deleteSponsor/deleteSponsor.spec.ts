process.env.SPONSORS_TABLE = 'sponsors'
import {deleteSponsor} from './deleteSponsor'
import {
  DynamoKey,
  DynamoSponsor,
  FakeDynamoDB
} from '../mock/dynamoDB'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'


describe('DELETE /sponsors:', function () {
  const inputEvent: APIGatewayProxyEvent = {pathParameters: {id: '1'}} as unknown as APIGatewayProxyEvent
  let fakeDynamoDB: FakeDynamoDB<DynamoSponsor>
  beforeEach(() => {
    fakeDynamoDB = new FakeDynamoDB()
  })

  describe('when an item exists', () => {
    const SPON_KEY: DynamoKey = {id: {S: '1'}}
    const SPONSOR: DynamoSponsor = {
      id: {S: '1'},
      name: {S: 'Some Co.'},
      contactName: {S: 'somebody'},
      contactPhone: {S: '0123456789'},
      logo: {S: 'http://google.de'},
      contactEmail: {S: 'some@body.com'}
    }
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      fakeDynamoDB._items.set(JSON.stringify(SPON_KEY), SPONSOR)
      result = await deleteSponsor(fakeDynamoDB as unknown as DynamoDB)(inputEvent)
    })
    it('should delete from table "sponsors"', function () {
      expect(fakeDynamoDB.table).toEqual('sponsors')
    })
    it('should return 200', async () => {
      expect(result.statusCode).toEqual(200)
    })
  })

  describe('when an item does not exist', () => {
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      result = await deleteSponsor(fakeDynamoDB as unknown as DynamoDB)(inputEvent)
    })


    it('should return 404', () => {
      expect(result.statusCode).toEqual(404)
    })
  })


})