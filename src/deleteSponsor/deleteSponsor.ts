import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  failure,
  success,
} from '../common/util'

const tableName = process.env.SPONSORS_TABLE

export const deleteSponsor = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const id = event?.pathParameters?.id
  if (id !== undefined) {
    try {
      await dynamoDB.deleteItem({TableName: tableName, Key: {id: {S: id}}})
      return success()
    } catch (e) {
      return e.__type.endsWith('ResourceNotFoundException')
             ? failure(404, e.message)
             : failure(500, e.message)
    }
  } else {
    return failure(400, 'Invalid parameters.')
  }
}