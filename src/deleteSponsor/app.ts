import {deleteSponsor} from './deleteSponsor'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

export const handler = deleteSponsor(new DynamoDB({region: 'eu-central-1'}))
