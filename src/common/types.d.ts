export type UUID = string
export type Sponsor = {
  id: UUID,
  name: string,
  contactName: string;
  contactEmail: string,
  contactPhone: string,
  logo: string
}

export type Slot = 'Gold' | 'Silver' | 'Bronze'

export type SponsorSlot = {
  conference: UUID,
  id: UUID,
  name: string,
  logo: string,
  slot: Slot;
  donation: string,
  paymentReceived: boolean
}