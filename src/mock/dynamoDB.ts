import {ScanOutput} from '@aws-sdk/client-dynamodb'

export type DynamoString = { S: string }
export type DynamoBoolean = {BOOL: boolean }
export type DynamoKey = { id: DynamoString }
export type DynamoSponsor = DynamoKey & { 
  name: DynamoString
  contactName: DynamoString
  contactEmail: DynamoString
  contactPhone: DynamoString
  logo: DynamoString
}
export type DynamoSlot = DynamoKey & {
  conference: DynamoString
  name: DynamoString
  slot: DynamoString
  donation: DynamoString
  paymentReceived: DynamoBoolean
  logo: DynamoString
}


export class FakeDynamoDB<T extends DynamoKey> {
  public _items: Map<string, T> = new Map()
  public table: String
  private _keyAttributes: string[]

  constructor(keyAttributes: string[] = ['id']) {
    this._keyAttributes = keyAttributes
  }

  async putItem(props: { TableName: string, Item: T }) {
    const {TableName, Item} = props
    this.table = TableName
    const key = this._keyAttributes.reduce((item, att) => {
      item[att] = Item[att]
    return item
    }, {})
    this._items.set(JSON.stringify(key), Item)
  }

  async getItem(props: { TableName: string, Key: DynamoKey }) {
    const {TableName, Key} = props
    this.table = TableName
    return this._items.get(JSON.stringify(Key))
  }

  async deleteItem(props: { TableName: string, Key: DynamoKey }) {
    const {TableName, Key} = props
    this.table = TableName
    const serializedKey: string = JSON.stringify(Key)
    if (this._items.has(serializedKey)) {
      return this._items.delete(serializedKey)
    } else {
      const error: Error = new Error()
      // @ts-ignore
      error.__type = 'com.amazonaws.dynamodb.v20120810#ResourceNotFoundException'
      error.message = `Requested resource not found: Item: ${Key.id} not found`
      throw error
    }
  }

  async scan({TableName}): Promise<ScanOutput> {
    this.table = TableName
    return {Items: [...this._items.values()] as any[]}
  }
}