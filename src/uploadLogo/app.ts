import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  failure,
  success
} from '../common/util'
import {S3} from '@aws-sdk/client-s3'
import {Upload} from '@aws-sdk/lib-storage'

const Bucket: string = process.env.UPLOAD_BUCKET

export const handler = (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const {file, filename, mimetype: ContentType} = JSON.parse(event.body)
  const fileContent: any = file.split(/;base64,/).pop()
  const Body = Buffer.from(fileContent, 'base64')
  const client: S3 = new S3({region: 'eu-central-1'})
  const ContentLength: number = Buffer.byteLength(fileContent, 'base64')
  return new Upload({
    client,
    params: {
      Body,
      ContentLength,
      ACL:'public-read',
      Bucket,
      Key: `logos/${filename}`,
      ContentType
    }
  })
    .done()
    .then(result => {
      return success(result)
    })
    .catch(err => failure(400, err))
}