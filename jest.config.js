module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  "testResultsProcessor": "jest-junit"
};